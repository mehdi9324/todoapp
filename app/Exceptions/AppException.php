<?php


namespace App\Exceptions;

use Exception;

class AppException extends Exception
{
    protected $messages;
    protected $exception = null;
    protected $exceptionCode = '';

    public function __construct($exceptionConst)
    {
        $this->exception = $this->messages[$exceptionConst];
        $this->exceptionCode = $exceptionConst;

        $this->code = $this->exception[0];
        $this->message = $this->exception[1];
    }

    public function getAppCode()
    {
        return $this->exceptionCode;
    }
}
