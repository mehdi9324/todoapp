<?php


namespace App\Exceptions;


class AuthException extends AppException
{
    CONST EMAIL_SENDING_ERROR = 'auth_email_sending_error';
    CONST CREDENTIALS_INVALID = 'auth_credentials_invalid';
    CONST CURRENT_PASSWORD_INVALID = 'auth_current_password_invalid';
    CONST OLD_AND_NEW_PASSWORD_ARE_SAME = 'auth_old_and_new_password_are_same';
    CONST AUTH_TOKEN_INVALID = 'auth_token_invalid';
    CONST FORCE_RESET_PASSWORD = 'auth_force_reset_password';
    CONST NOT_MERCHANT = 'auth_user_is_not_merchant';

    protected $messages ;

    public function __construct($exceptionConst) {
        $this->messages = [
            'auth_email_sending_error' => [409, __('exceptions.auth_email_sending_error')],
            'auth_credentials_invalid' => [401, __('exceptions.auth_credentials_invalid')],
            'auth_current_password_invalid' => [400, __('exceptions.auth_current_password_invalid')],
            'auth_old_and_new_password_are_same' => [400, __('exceptions.auth_old_and_new_password_are_same')],
            'auth_token_invalid' => [401, __('exceptions.auth_token_invalid')],
            'auth_force_reset_password' => [400, __('exceptions.auth_force_reset_password')],
            'auth_user_is_not_merchant' => [401, __('exceptions.auth_user_is_not_merchant')],
        ];

        parent::__construct($exceptionConst);
    }

}
