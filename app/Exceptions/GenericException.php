<?php


namespace App\Exceptions;


class GenericException extends AppException
{
    CONST INTERNAL_SERVER_ERROR = 'internal_server_error';
    CONST UNHANDLED_EXCEPTION = 'unhandled_exception';
    CONST VALIDATION_ERROR = 'validation_error';
    CONST CONTENT_TYPE_INVALID = 'content_type_invalid';
    CONST DATA_FORMAT_INVALID = 'data_format_invalid';
    CONST REQUEST_INVALID = 'request_invalid';
    CONST NOT_FOUND = 'not_found';
    CONST MAINTENANCE_MODE = 'maintenance_mode';
    CONST RATE_LIMIT_REACHED = 'rate_limit_reached';
    CONST PAGE_NOT_FOUND = 'page_not_found';
    CONST HTTP_RESPONSE_CODE_INVALID = 'http_response_code_invalid';
    CONST PSP_DISABLED = 'psp_disabled';

    protected $messages;

    public function __construct($exceptionConst)
    {

        $this->messages = [
            'internal_server_error' => [500, __('exceptions.internal_server_error')],
            'unhandled_exception' => [500, __('exceptions.unhandled_exception')],
            'validation_error' => [401, __('exceptions.validation_error')],
            'content_type_invalid' => [400, __('exceptions.content_type_invalid')],
            'data_format_invalid' => [400, __('exceptions.data_format_invalid')],
            'request_invalid' => [400, __('exceptions.request_invalid')],
            'not_found' => [404, __('exceptions.not_found')],
            'maintenance_mode' => [503, __('exceptions.maintenance_mode')],
            'rate_limit_reached' => [429, __('exceptions.rate_limit_reached')],
            'page_not_found' => [404, __('exceptions.page_not_found')],
            self::HTTP_RESPONSE_CODE_INVALID => [422, __('exceptions.'.self::HTTP_RESPONSE_CODE_INVALID)],
            self::PSP_DISABLED => [404, __('exceptions.'.self::PSP_DISABLED)],

        ];
        parent::__construct($exceptionConst);
    }
}
