<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Exceptions\AuthException;
use App\Http\Controllers\AppBaseController;
use App\Http\Request\API\User\UserLoginApiRequest;
use App\Services\UserServiceInterface;

class AuthController extends AppBaseController
{
    protected $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;

    }

    public function login(UserLoginApiRequest $request)
    {
        $email = $request->email;
        $password = $request->password;
        if (!$token = auth('api')->attempt(["email" => $email, "password" => $password])) {
            abort(401, AuthException::CREDENTIALS_INVALID);
        }
        $user = $this->userService->findByColumn('email', $email);
        $token = auth('api')->login($user);

        return response()->json([
            'user_id' => $user->id,
            'token' => $token,
            'expires_in' => auth('api')->factory()->getTTL() * 1000000
        ],
            200);
    }


    public function logout()
    {
        auth('api')->logout();
        return response()->json([
            'message' => 'User logged out'
        ],
            200);
    }
}
