<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\AppBaseController;
use App\Http\Request\API\Task\TaskApiCreateRequest;
use App\Http\Request\API\Task\TaskApiUpdateRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Models\User;
use App\Notifications\CreateTaskNotification;
use App\Services\API\TaskApiServiceInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class TaskApiController extends AppBaseController
{

    protected $taskApiService;

    public function __construct(TaskApiServiceInterface $taskApiService)
    {
        $this->taskApiService = $taskApiService;
    }

    /**
     *
     * @param null $qry
     * @return JsonResponse
     */
    public function index($qry = null)
    {
        if (request()->has('search')) {
            $qry= request()->input('search');
        }
        $list = $this->taskApiService->index($qry);

        return $this->sendResponse(TaskResource::collection($list));
    }
    public function indexPersonal()
    {
        $user = Auth::user();
        $list = $this->taskApiService->indexPersonal($user);

        return $this->sendResponse(TaskResource::collection($list));
    }

    public function create(TaskApiCreateRequest $taskApiStoreRequest)
    {
        $user  = Auth::user();
        $taskApiStoreRequest->merge(['user_id'=> $user->getAuthIdentifier()]);

        #create new task
        $data = $this->taskApiService->create($taskApiStoreRequest->all());

        #send notify
        $user->notify(new CreateTaskNotification($user));
        return $this->sendResponse($data,'data created successful');
    }
    public function update(Task $task,TaskApiUpdateRequest $taskApiUpdateRequest)
    {
        #check permission for update
        $permission =  $this->hasPermissionToModifying($task);
        if ($permission == false) {
            $functionName = (__FUNCTION__);
            return $this->sendError("you are not access for $functionName this item", 403);
        }
        $response = $task->update($taskApiUpdateRequest->all());
        return $this->sendResponse([$response],'data updated successful');
    }
    public function delete(Task $task)
    {
        #check permission for delete
        $permission =  $this->hasPermissionToModifying($task);
        if ($permission == false) {
            $functionName = (__FUNCTION__);
            return $this->sendError("you are not access for $functionName this item", 403);
        }
        $response = $task->delete();
        return $this->sendResponse([$response],'data deleted successful');
    }

    /**
     * @param Task $task
     * @return bool|JsonResponse
     */
    public function hasPermissionToModifying(Task $task)
    {
        $user = Auth::user();
        # check 3 condition for give modify permission
        if ($user->role == User::ROLE['USER'] and ($task->user_id != $user->id) and ($task->is_private == 1)) {
            return  false;
        }
            return true;
    }

}
