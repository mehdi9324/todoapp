<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;
use Response;
/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($data,$message ='data retrieved successfully' , $code=200 )
    {
        return response()->json([
            'success'=>true,
            'code' => $code,
            'message'=>$message,
            'data'=>$data,
        ]);
    }

    public function sendError($error, $code = 404)
    {
        return response()->json([
            'success'=>false,
            'code' => $code,
            'message'=>$error,
        ]);
    }
}
