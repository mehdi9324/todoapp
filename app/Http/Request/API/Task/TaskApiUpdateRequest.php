<?php

namespace App\Http\Request\API\Task;

use Illuminate\Foundation\Http\FormRequest;


class TaskApiUpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|required|min:4|max:190',
            'description' => 'bail|required|min:4',
            'due_date' => 'bail|date_format:Y-m-d H:i:s',
            "priority" => 'in:LOW,MEDIUM,HIGH',
            "is_private" => 'numeric',
        ];
    }
}
