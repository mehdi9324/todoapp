<?php

namespace App\Http\Request\API\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed from
 * @property mixed to
 * @property mixed currency_id
 * @property mixed type
 */
class UserLoginApiRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => 'required|email|exists:users,email',
            "password" => 'required',
        ];
    }
}
