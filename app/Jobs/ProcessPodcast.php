<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessPodcast implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $podcast;
    private $tries = 5;
    /**
     * Create a new job instance.
     *
     * @param $podcast
     */
    public function __construct($podcast)
    {
        $this->podcast = $podcast;
    }


    public function handle($podcast)
    {
        return ($podcast);
    }
}
