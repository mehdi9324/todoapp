<?php

namespace App\Providers;

use App\Services\API\TaskApiService;
use App\Services\API\TaskApiServiceInterface;
use App\Services\API\PriceApiService;
use App\Services\API\PriceApiServiceInterface;
use App\Services\API\ProviderApiService;
use App\Services\API\ProviderApiServiceInterface;
use App\Services\UserService;
use App\Services\UserServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->injectServices();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function injectServices()
    {
        $this->app->bind(UserServiceInterface::class, UserService::class);
        $this->app->bind(TaskApiServiceInterface::class, TaskApiService::class);

    }
}
