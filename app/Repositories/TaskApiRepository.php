<?php


namespace App\Repositories;


use App\Models\Task;

class TaskApiRepository extends BaseRepository
{

    protected $fieldSearchable = [];

    /**
     * Get searchable fields array
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    public function index($qry)
    {
        $query = $this->allQuery();
        # search
        if (isset($qry)) {
            $query->where('title', 'like', $qry);
        }
        return $query->orderByDesc("created_at")->get();
    }

    public function indexPersonal( $user)
    {
        $query = $this->allQuery()->where('user_id',$user->id);

        return $query->orderByDesc("created_at")->get();
    }

    /**
     * Configure the Model
     *
     * @return string
     */
    public function model()
    {
        return Task::class;
    }
}
