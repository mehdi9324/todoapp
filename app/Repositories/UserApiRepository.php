<?php

namespace App\Repositories;


use App\Models\User;


class UserApiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
    ];


    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    public function model()
    {
        return User::class;
    }

    public function findByColumn($column, $data)
    {
        return $this->allQuery()->where($column, $data)->first();
    }


}
