<?php


namespace App\Services\API;

use App\Repositories\TaskApiRepository;

class TaskApiService implements TaskApiServiceInterface
{
    protected $taskApiRepository;

    public function __construct(TaskApiRepository $taskApiRepository)
    {
        $this->taskApiRepository = $taskApiRepository;
    }

    public function index($qry)
    {
        return  $this->taskApiRepository->index($qry);
    }
    public function indexPersonal($user)
    {
        return  $this->taskApiRepository->indexPersonal($user);
    }

    public function create($input)
    {
        return  $this->taskApiRepository->create($input);
    }
}
