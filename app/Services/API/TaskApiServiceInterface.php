<?php


namespace App\Services\API;

use Illuminate\Support\Facades\Auth;

interface TaskApiServiceInterface
{
    public function index($qry);

    public function indexPersonal(Auth $user);

    public function create($input);

}
