<?php


namespace App\Services;


use App\Repositories\UserApiRepository;
use Carbon\Carbon;

class UserService extends BaseService implements UserServiceInterface
{
    /**
     * @var UserApiRepository
     */
    protected $userRepository;


    public function __construct (UserApiRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function createAdmin($data)
    {
        $this->userRepository->create($data);
    }

    public function findByColumn($column, $data)
    {
       return $this->userRepository->findByColumn($column, $data);

    }

    public function changePassword($user, $password)
    {
       $input = [
           'password'=>bcrypt($password)
       ];
        $this->userRepository->update($input, $user->id);
    }
}
