<?php


namespace App\Services;

use App\Models\User;

interface UserServiceInterface
{
    public function findByColumn($column, $data);

    public function changePassword($user, $password);
}
