<?php
namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @param Factory $faker
     */

    public function run()
    {
        $faker = Factory::create();

        $users = [
            [
                'name' => 'mehdi',
                'family' => 'hoseini',
                'role' => User::ROLE['ADMIN'],
                'email' => 'mehdi.hoseini86@gmail.com',
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],
            [
                'name' => 'mostafa',
                'family' => 'khalilnasab',
                'role' => User::ROLE['ADMIN'],
                'email' => 'mostafa@gmail.com',
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],[
                'name' => $faker->name,
                'family' => $faker->lastName,
                'role' => User::ROLE['USER'],
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' =>bcrypt('123456'),
                'active' => 1,
            ],
            ];
        foreach ($users as $user) {
            User::firstOrCreate([
                'email' => $user['email']
            ], $user);
        }
    }
}
