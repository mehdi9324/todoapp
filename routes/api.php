<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Auth'], function () {
    Route::post('login', 'AuthController@login');

});
Route::group(['middleware' => ['jwt.auth'], 'namespace' => 'Auth'], function () {
    Route::post('logout', 'AuthController@logout');
});

Route::group(['middleware' => ['jwt.auth'],'prefix' => 'task'], function () {
    Route::get('/', 'TaskApiController@index');
    Route::get('/personal', 'TaskApiController@indexPersonal');
    Route::post('create', 'TaskApiController@create');
    Route::put('{task}', 'TaskApiController@update');
    Route::delete('{task}', 'TaskApiController@delete');
});
