<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $users = App\Models\User::all();

    $bar = $this->output->createProgressBar(count($users));


    $bar->start();

    foreach ($users as $user) {
//        $this->performTask($user);
sleep(1);
        $bar->advance();
    }

    $bar->finish();
})->purpose('Display an inspiring quote');

Artisan::command('build {project}', function ($project) {
    $name = $this->choice(
        'What is your name?',
        ['Taylor', 'Dayle'],
        $defaultIndex =1,
        $maxAttempts = null,
        $allowMultipleSelections = true
    );
})->describe('Build the project');
